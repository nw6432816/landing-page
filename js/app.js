const dialog = document.querySelector("dialog");
const cartButton = document.getElementById("add__to__cart");
const dialogButton = document.getElementById("close__dialog");

cartButton.addEventListener("click", () => {
    dialog.showModal();
});

dialogButton.addEventListener("click", () => {
    dialog.close();
});